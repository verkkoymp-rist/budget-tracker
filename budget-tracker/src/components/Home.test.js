import React from 'react';
import { render, screen } from '@testing-library/react';
import Home from './Home';

describe('Home Component', () => {
  test('renders Home component with balance', () => {
    render(<Home />);
    expect(screen.getByText(/Current Balance:/i)).toBeInTheDocument();
  });
});
