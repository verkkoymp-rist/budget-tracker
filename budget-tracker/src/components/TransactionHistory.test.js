import React from 'react';
import { render, screen } from '@testing-library/react';
import TransactionHistory from './TransactionHistory';

describe('TransactionHistory Component', () => {
  test('displays transactions if any', () => {
    const transactions = [{ type: 'income', amount: 100 }];
    localStorage.setItem('transactions', JSON.stringify(transactions));
    render(<TransactionHistory />);
    expect(screen.getByText(/income: \$100/i)).toBeInTheDocument();
    localStorage.clear(); 
  });
});
