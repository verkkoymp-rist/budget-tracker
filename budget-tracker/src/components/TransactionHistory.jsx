import React, { useState, useEffect } from 'react';
import './TransactionHistory.css';

const TransactionHistory = () => {
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    loadTransactions();
  }, []);

  const loadTransactions = () => {
    const storedTransactions = JSON.parse(localStorage.getItem('transactions')) || [];
    setTransactions(storedTransactions);
  };

  const deleteTransaction = (index) => {
    let updatedTransactions = [...transactions];
    updatedTransactions.splice(index, 1);
    localStorage.setItem('transactions', JSON.stringify(updatedTransactions));
    loadTransactions();  // Refresh list after deletion
  };

  return (
    <div className="transaction-history">
      <h1>Transaction History</h1>
      <ul>
        {transactions.map((transaction, index) => (
          <li key={index}>
            {transaction.type}: ${transaction.amount.toFixed(2)}
            <button onClick={() => deleteTransaction(index)}>Delete</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default TransactionHistory;
