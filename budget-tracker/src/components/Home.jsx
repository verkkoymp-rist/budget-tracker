import React, { useState, useEffect } from 'react';
import './Home.css';

const Home = () => {
  const [balance, setBalance] = useState(0);

  useEffect(() => {
    const transactions = JSON.parse(localStorage.getItem('transactions')) || [];
    const totalBalance = transactions.reduce((acc, curr) => {
      return curr.type === 'income' ? acc + curr.amount : acc - curr.amount;
    }, 0);
    setBalance(totalBalance);
  }, []);

  return (
    <div className="home">
      <h1>Home Page</h1>
      <h2>Current Balance: ${balance}</h2>
    </div>
  );
};

export default Home;
