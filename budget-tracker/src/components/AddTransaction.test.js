import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import AddTransaction from './AddTransaction';

describe('AddTransaction Component', () => {
  test('allows the user to add a transaction', () => {
    render(<AddTransaction />);
    fireEvent.change(screen.getByRole('textbox', {name: /amount/i}), { target: { value: '23' } });
    fireEvent.click(screen.getByText(/Add Transaction/i));
    expect(screen.getByText(/Transaction added!/i)).toBeInTheDocument();
  });
});
