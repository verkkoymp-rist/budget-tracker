import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Menu from './Menu/Menu';
import Home from './components/Home';
import AddTransaction from './components/AddTransaction';
import TransactionHistory from './components/TransactionHistory';

function App() {
  return (
    <Router>
      <div>
        <Menu />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/add" element={<AddTransaction />} />
          <Route path="/history" element={<TransactionHistory />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
