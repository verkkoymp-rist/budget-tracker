import React from 'react';
import { Link } from 'react-router-dom';
import './Menu.css';

const Menu = () => {
  return (
    <div className="menu">
      <Link to="/">Home</Link>
      <Link to="/add">Add Transaction</Link>
      <Link to="/history">Transaction History</Link>
    </div>
  );
};

export default Menu;
